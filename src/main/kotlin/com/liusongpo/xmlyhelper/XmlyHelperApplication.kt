package com.liusongpo.xmlyhelper

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

@SpringBootApplication
class XmlyHelperApplication {

    @Bean
    fun executor(): ThreadPoolTaskExecutor {
        val executor = ThreadPoolTaskExecutor()
        executor.corePoolSize = 512
        return executor
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<XmlyHelperApplication>(*args)
        }
    }

}
