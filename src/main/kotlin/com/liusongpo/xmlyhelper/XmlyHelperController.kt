package com.liusongpo.xmlyhelper

import com.alibaba.fastjson.JSONObject
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
class XmlyHelperController(private val service: XmlyHelperService) {

    @GetMapping("/")
    fun index(): String {
        return "index"
    }

    @GetMapping("/download")
    fun download(category: String, albumId: String, title: String, model: Model): String {
        model.addAttribute("albumId", albumId)
        model.addAttribute("category", category)
        model.addAttribute("title", title)
        return "download"
    }

    /**
     * 查询歌曲列表
     */
    @GetMapping("/search")
    @ResponseBody
    fun search(name: String, pageNum: Int = 1, pageSize: Int = 20): JSONObject {
        return service.search(name, pageNum, pageSize)
    }

    /**
     * 查询专辑信息
     */
    @GetMapping("/album")
    @ResponseBody
    fun album(category: String, albumId: String, pageNum: Int, pageSize: Int): JSONObject {
        return service.searchAlbum(category, albumId, pageNum, pageSize)
    }

    /**
     * 查询歌曲列表
     */
    @GetMapping("/tracks")
    @ResponseBody
    fun tracks(category: String, albumId: String): JSONObject {
        return service.searchTracks(category, albumId)
    }

    /**
     * 查询歌曲信息
     */
    @GetMapping("/track")
    @ResponseBody
    fun track(category: String, albumId: String, trackId: String): JSONObject {
        return service.searchTrack(category, albumId, trackId)
    }

    @PostMapping("/download")
    @ResponseBody
    fun download(@RequestBody data: JSONObject) {
        this.service.download(data)
    }
}
