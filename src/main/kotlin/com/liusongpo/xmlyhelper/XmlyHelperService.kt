package com.liusongpo.xmlyhelper

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import mu.KotlinLogging
import org.apache.commons.io.FileUtils
import org.apache.http.client.fluent.Request
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Service
import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.nio.file.Files

@Service
class XmlyHelperService(private val executor: ThreadPoolTaskExecutor) {

    private val logger = KotlinLogging.logger {}

    fun search(name: String, pageNum: Int, pageSize: Int): JSONObject {
        logger.debug("搜索歌曲，歌曲名称：$name")
        var data = JSONObject()
        try {
            val categoryUrl = "$BASE_URL/revision/search?core=album&kw=${URLEncoder.encode(name, "UTF-8")}&page=$pageNum&spellchecker=true&rows=$pageSize&condition=relation&device=iPhone"
            val result = Request.Get(categoryUrl)
                    .addHeader("Accept", "*/*")
                    .addHeader("Accept-Encoding", "gzip, deflate, br")
                    .addHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                    .addHeader("DNT", "1")
                    .addHeader("Host", "www.ximalaya.com")
                    .addHeader("Pragma", "no-cache")
                    .addHeader("Referer", "$BASE_URL/search/${URLEncoder.encode(name, "UTF-8")}/sc/p1")
                    .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                    .execute()
                    .returnContent()
                    .asString(StandardCharsets.UTF_8)
            data = JSON.parseObject(result).getJSONObject("data").getJSONObject("result").getJSONObject("response")
        } catch (e: IOException) {
            logger.error("解析分类失败，{}", e)
        }
        return data
    }

    fun searchAlbum(category: String, albumId: String, pageNum: Int = 1, pageSize: Int = 999): JSONObject {
        logger.debug("搜索专辑，专辑类别：$category，专辑标识：$albumId")
        var data = JSONObject()
        try {
            val categoryUrl = "$BASE_URL/revision/play/album?albumId=$albumId&pageNum=$pageNum&sort=0&pageSize=$pageSize"
            val result = Request.Get(categoryUrl)
                    .addHeader("Accept", "*/*")
                    .addHeader("Accept-Encoding", "gzip, deflate, br")
                    .addHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                    .addHeader("DNT", "1")
                    .addHeader("Host", "www.ximalaya.com")
                    .addHeader("Pragma", "no-cache")
                    .addHeader("Referer", "$BASE_URL/$category/$albumId/")
                    .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                    .execute()
                    .returnContent()
                    .asString(StandardCharsets.UTF_8)
            val json = JSON.parseObject(result)
            if (json.containsKey("data")) {
                data = json.getJSONObject("data")
            } else {
                logger.warn { "获取数据失败，$json" }
            }
        } catch (e: IOException) {
            logger.error("解析分类失败，{}", e)
        }
        return data
    }

    fun searchTracks(category: String, albumId: String): JSONObject {
        logger.debug("搜索歌曲列表，专辑标识：$albumId")
        var data = JSONObject()
        try {
            val categoryUrl = "$BASE_URL/revision/album/getTracksList?albumId=$albumId&pageNum=1&sort=0"
            val result = Request.Get(categoryUrl)
                    .addHeader("Accept", "*/*")
                    .addHeader("Accept-Encoding", "gzip, deflate, br")
                    .addHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                    .addHeader("DNT", "1")
                    .addHeader("Host", "www.ximalaya.com")
                    .addHeader("Pragma", "no-cache")
                    .addHeader("Referer", "$BASE_URL/$category/$albumId/")
                    .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                    .execute()
                    .returnContent()
                    .asString(StandardCharsets.UTF_8)
            data = JSON.parseObject(result).getJSONObject("data")
        } catch (e: IOException) {
            logger.error("解析歌曲列表失败，{}", e)
        }
        return data
    }

    fun searchTrack(category: String, albumId: String, trackId: String): JSONObject {
        logger.debug("搜索歌曲信息，歌曲标识：$trackId")
        var data = JSONObject()
        try {
            val categoryUrl = "$BASE_URL/revision/play/tracks?trackIds=$trackId"
            val result = Request.Get(categoryUrl)
                    .addHeader("Accept", "*/*")
                    .addHeader("Accept-Encoding", "gzip, deflate, br")
                    .addHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                    .addHeader("DNT", "1")
                    .addHeader("Host", "www.ximalaya.com")
                    .addHeader("Pragma", "no-cache")
                    .addHeader("Referer", "$BASE_URL/$category/$albumId/$trackId")
                    .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                    .execute()
                    .returnContent()
                    .asString(StandardCharsets.UTF_8)
            data = JSON.parseObject(result).getJSONObject("data").getJSONObject("tracksForAudioPlay")
        } catch (e: IOException) {
            logger.error("解析歌曲失败，{}", e)
        }
        return data
    }

    fun download(data: JSONObject) {
        val albumName = data.getString("albumName")
        val tracks = data.getJSONArray("tracks")
        // 总任务数
        val totalCount = tracks.size
        // 已完成任务数
        var completeCount = 0

        tracks.forEach {
            val json = it as JSONObject
            val filename = json.getString("name") + ".m4a"
            val file = File("$BASE_SAVE_DIR/$albumName/$filename")

            logger.debug("开始下载文件，名称：${file.absolutePath}，地址：${json.getString("url")}")

            // 获取服务器文件大小
            val serverFileLength = Request.Head(json.getString("url")).execute().returnResponse().getLastHeader("Content-Length").value
            // 检测文件是否已经存在和件大小是否符合
            if (!Files.exists(file.toPath()) && file.length() < serverFileLength.toLong()) {
                val thread = Thread {
                    try {
                        val bytes = Request.Get(json.getString("url"))
                                .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                                .addHeader("Accept-Encoding", "gzip, deflate")
                                .addHeader("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6")
                                .addHeader("Cache-Control", "no-cache")
                                .addHeader("Connection", "keep-alive")
                                .addHeader("DNT", "1")
                                .addHeader("Host", "audio.xmcdn.com")
                                .addHeader("Pragma", "no-cache")
                                .addHeader("Upgrade-Insecure-Requests", "1")
                                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                                .execute()
                                .returnContent().asBytes()

                        FileUtils.writeByteArrayToFile(file, bytes)
                        logger.debug("总进度 ${++completeCount}/$totalCount, 下载 ${file.name} 成功，路径：${file.absolutePath}")
                    } catch (e: Exception) {
                        logger.error("下载失败，名称：$filename，地址：${json.getString("url")}，原因：$e")
                    }
                }

                executor.submit(thread)
            } else {
                logger.debug { "总进度 ${++completeCount}/$totalCount，文件 $filename 已下载，跳过下载流程" }
            }
        }
    }

    companion object {

        private const val BASE_URL = "https://www.ximalaya.com"

        private const val BASE_SAVE_DIR = "download"
    }
}
